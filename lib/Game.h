#include "SDL.h"
#include "algorithm"

#include "./Object.h"
#include "./objects/Player.h"
#include "./objects/Background.h"

#include <iostream>
#include <vector>

using namespace std;

class Game {
  private:
    /** Window object used to render the game */
    SDL_Window* window;

    /** Renderer object used to render the game */
    SDL_Renderer* renderer;

    /** The title of the game */
    string title = "Test Game";

    /** Sort objects first by the Z position and then by the Y position */
    static bool sort_objects (Object* i, Object* j) {
      return i->z < j->z || i->y + i->height < j->y + j->height;
    }

  public:
    Player player;
    Object camera;
    Background background;
    vector<Object*> objects;    

    /**
     * Game object containing and managing the game related functions
     * Upon initialisation, it creates the game window and the renderer. 
    */
    Game () {
      SDL_Init(SDL_INIT_EVERYTHING);

      window = SDL_CreateWindow(title.c_str(), 
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, 
                                640, 480, 
                                SDL_WINDOW_RESIZABLE);

      renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

      IMG_Init(IMG_INIT_PNG);

      add_object(&player);
      background.load_texture(renderer);
    }

    /** Upon destruction, quit SDL and destroy the window */
    ~Game () {
      SDL_DestroyWindow(window);
      SDL_Quit();
    }

    /** 
     * Get the size of the game's window and set the result to the provided parameters 
     * @param x - the pointer to an X integer
     * @param y - the pointer to an Y integer
    */
    void get_window_size (int* x, int* y) {
      SDL_GetWindowSize(window, x, y);
    }

    /**
     * Add given object to the end of the list and return it
     * @param object - an object to push to the list 
    */
    Object* add_object (Object* object) {
      if (object->path.size() > 0)
        object->load_texture(renderer);

      objects.push_back(object);
      return object;
    }

    void render_background () {
      int win_x, win_y;
      get_window_size(&win_x, &win_y);

      for (int x = 0; x < win_x; x += background.width) {
        for (int y = 0; y < win_y; y += background.height) {
          int angle = ((x + y) % 3) * 90;
          SDL_Rect rect = { x, y, background.width, background.height };
          SDL_Point center = { background.width / 2, background.height / 2 };

          SDL_RenderCopyEx(renderer, background.texture, NULL, &rect, angle, &center, SDL_FLIP_VERTICAL);
        }
      }
    }

    /** Render a square on the game window */
    void render () {
      int x, y;
      get_window_size(&x, &y);

      camera.x = player.x + player.width / 2 - x / 2;
      camera.y = player.y + player.height / 2 - y / 2;

      vector<Object*> sorted = this->objects;
      sort(sorted.begin(), sorted.end(), sort_objects);

      // Set background color and clear the window
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
      SDL_RenderClear(renderer);
      this->render_background();

      // Render the objects
      for (Object* o : sorted) {
        SDL_Rect rect = { o->x - camera.x, o->y - camera.y, o->width, o->height };

        if (o->texture != NULL) {
          SDL_RenderCopy(renderer, o->texture, NULL, &rect);
        } else {
          SDL_SetRenderDrawColor(renderer, o->color.r, o->color.g, o->color.b, o->color.a);
          SDL_RenderFillRect(renderer, &rect);
        }
      }
      
      // Render up to date presentation of the game
      SDL_RenderPresent(renderer);
    }

    /** Handle the user's input in order to move the player */
    void handle_input () {
      const Uint8* state = SDL_GetKeyboardState(NULL);

      if (state[SDL_SCANCODE_RIGHT])
        player.move(5, 0);
      else if (state[SDL_SCANCODE_LEFT])
        player.move(-5, 0);

      if (state[SDL_SCANCODE_UP])
        player.move(0, -5);
      else if (state[SDL_SCANCODE_DOWN])
        player.move(0, 5);
    }

    /** Check collisions of the player with another objects */
    void check_collisions () {
      for (Object* o : objects) {
        if (!o->collider_enabled)
          continue;

        for (Object* o1 : objects) {
          if (o == o1)
            continue;

          bool collision = o->check_collision(*o1);
          
          if (collision)
            o->on_collision(o1);
          else
            o->on_collision_leave(o1);
        }
      }
    }
};