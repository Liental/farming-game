#ifndef OBJECT_H
#define OBJECT_H

#include "SDL.h"
#include <SDL2/SDL_image.h>

#include <vector>
#include <iostream>

using namespace std;

/** Representation of an object in the game */
class Object {
  public:
    /** The X position of the object */
    int x = 0;

    /** The Y position of the object */
    int y = 0;

    /** The Z position of the object */
    int z = 0;

    /** The height of the object */
    int height = 128 * .75;

    /** The width of the object */
    int width = 96 * .75;

    /** The path to the object's texture */
    string path = "";

    /** A vector containing the tags of an objects */
    vector<string> tags;

    /** Determines if the object should have on collide events or not */
    bool collider_enabled = false;

    /** The color of the object's rectangle */
    SDL_Color color = { 255, 255, 255, 255 };

    /** The surface of the object's texture */
    SDL_Texture* texture = NULL;
    
    /**
     * Load the texture of the object if it has a path
     * @param renderer - the renderer of the game
    */
    void load_texture (SDL_Renderer* renderer) {
      SDL_Surface* surface = IMG_Load(this->path.c_str());
      this->texture = SDL_CreateTextureFromSurface(renderer, surface);
    }

    /**
     * Move the player vertically and horizontally to provided values
     * @param x - the X position value to move the player to
     * @param y - the Y position value to move the player to
    */
    void move (int x, int y) {
      this->x = x;
      this->y = y;
    }

    /**
     * Check if the object is colliding with provided object based on the position and the size
     * @param o - an object to check the collision with
    */
    bool check_collision (Object o) {
      int is_x = o.x + o.width >= this->x && o.x < this->x + this->width;
      int is_y = o.y + o.height >= this->y && o.y < this->y + this->height;

      int is_x1 = this->x + this->width >= o.x && this->x < o.x + o.width;
      int is_y1 = this->y + this->height >= o.y && this->y < o.y + o.height;

      return (is_x && is_y) || (is_x1 && is_y1);
    }

    /** 
     * A function to execute on collision with another object
     * @param o - the object that collided with this object
    */
    virtual void on_collision (Object* o) {

    };

    /** 
     * A function to execute when a collision of another object ends
     * @param o - the object that collided with the player
    */
    virtual void on_collision_leave (Object* o) {

    };
};

#endif