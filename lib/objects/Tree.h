#include "../Object.h"

class Tree : public Object {
  public:
    /**
     * A class representing an object used for testing 
    */
    Tree () {
      this->width *= 2;
      this->height *= 2;
      this->tags = { "tree" };
      this->path = "./assets/tree.png";
    }
};
