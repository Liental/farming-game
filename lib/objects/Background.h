#include "../Object.h"

class Background : public Object {
  public:
    /** A representation of the background in the game */
    Background () {
      this->width = 64;
      this->height = 64;
      this->path = "./assets/background.png";
    }
};
