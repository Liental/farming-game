#include <iostream>
#include "../Object.h"

class Player : public Object {
  public:
    /** A representation of the Player in the game */
    Player () {
      this->tags = { "player" };
      this->collider_enabled = true;
      this->path = "./assets/character.png";
    }

    /**
     * Move the player vertically and horizontally by provided values
     * @param x - the X position value to move the player by
     * @param y - the Y position value to move the player by
    */
    void move (int x, int y) {
      this->x += x;
      this->y += y;
    }

    void on_collision (Object* o) {
      std::cout << "Collision detected: " << o->tags[0] << std::endl;
    }
};
