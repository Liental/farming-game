#include "../Object.h"

class Apple : public Object {
  public:
    /**
     * A class representing an object used for testing 
    */
    Apple () {
      this->width /= 3;
      this->height /= 4;
      this->tags = { "apple" };
      this->path = "./assets/apple.png";
    }
};
