#include <cstdlib>
#include <iostream>
#include <vector>

#include "./lib/Game.h"
#include "./lib/objects/Tree.h"
#include "./lib/objects/Apple.h"

using namespace std;

int main (int argc, char* argv[]) {
  Game game;
  vector<Object> objects;
  
  int window_x, window_y;
  game.get_window_size(&window_x, &window_y);

  srand(time(NULL));

  for (int i = 0; i < 5; i++) {
    Tree o;
    o.move(rand() % window_x, rand() % window_y);  
    
    objects.push_back(o);
  }

  for (int i = 0; i < 50; i++) {
    Apple o;
    o.move(rand() % window_x, rand() % window_y);  
    
    objects.push_back(o);
  }

  for (int i = 0; i < objects.size(); i++)
    game.add_object(&objects[i]);

  while (true) {
    bool quit_requested = SDL_QuitRequested();

    if (quit_requested)
      break;

    game.handle_input();
    game.check_collisions();
    game.render();

    string error = SDL_GetError();
    
    if (error.size() > 0)
      cout << error << endl;

    SDL_Delay(1000 / 30);
  }

  return 0;
}